
make clean
rm -f CMakeCache.txt
rm -rf CMakeFiles
cmake . -DCMAKE_VERBOSE_MAKEFILE=true -DCMAKE_CXX_FLAGS=-std=c++11 -G "Eclipse CDT4 - Unix Makefiles"
