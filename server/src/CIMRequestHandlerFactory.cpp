/*
 * CIMRequestHandlerFactory.cpp
 *
 *  Created on: Jul 29, 2016
 *      Author: ruelsison
 */

#include "CIMRequestHandlerFactory.h"
#include "HTTPRequestHandler.h"
#include "DefaultHTTPRequestHandler.h"

#include <cpprest/http_headers.h>

using namespace web;
using namespace web::http;

#include <iostream>

HTTPRequestHandler* CIMRequestHandlerFactory::createRequestHandler(
		http::http_request request)
{
//	std::ostringstream ss;
//	ss << "CIMRequestHandlerFactory::createRequestHandler" << std::endl;
//	ss << "request.relative_uri " << request.relative_uri().to_string() << std::endl;
//	std::cout << ss.str();
//
//	std::cout << "=== headers ===" << std::endl;
//	http_headers headers = request.headers();
//	std::for_each(std::begin(headers), std::end(headers),
//	[=](http_headers::const_reference kv)
//	{
//		std::cout << kv.first << ": " << kv.second << std::endl;
//	});

	//if (request.relative_uri() == "/")
	//	return new CIMRequestHandler(_format);
	//else
		return new DefaultHTTPRequestHandler();
}

