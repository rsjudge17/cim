/*
 * DefaultHTTPRequestHandler.h
 *
 *  Created on: Aug 1, 2016
 *      Author: ruelsison
 */

#ifndef SERVER_SRC_REQUEST_DEFAULTHTTPREQUESTHANDLER_H_
#define SERVER_SRC_REQUEST_DEFAULTHTTPREQUESTHANDLER_H_

#include <cpprest/http_listener.h>

using namespace web;
using namespace web::http;

#include "HTTPRequestHandler.h"

class DefaultHTTPRequestHandler: public HTTPRequestHandler {
public:
	DefaultHTTPRequestHandler();

	/**
	 * Callback method for HEAD request.
	 *
	 * @param request - the http request
	 */
	void handleHead(http::http_request request);

	/**
	 * Callback method for GET request.
	 *
	 * @param request - the http request
	 */
	void handleGet(http::http_request request);

	/**
	 * Callback method for PUT request.
	 *
	 * @param request - the http request
	 */
	void handlePut(http::http_request request);

	/**
	 * Callback method for POST request.
	 *
	 * @param request - the http request
	 */
	void handlePost(http::http_request request);

	/**
	 * Callback method for DELETE request.
	 *
	 * @param request - the http request
	 */
	void handleDelete(http::http_request request);

	/**
	 * Callback method for OPTIONS request.
	 *
	 * @param request - the http request
	 */
	void handleOptions(http::http_request request);

	/**
	 * Callback method for PATCH request.
	 *
	 * @param request - the http request
	 */
	void handlePatch(http::http_request request);

	/**
	 * Callback method for PATCH request.
	 *
	 * @param request - the http request
	 */
	void handleMerge(http::http_request request);

	/**
	 * Callback method for PATCH request.
	 *
	 * @param request - the http request
	 */
	void handleTrace(http::http_request request);

	/**
	 * Callback method for PATCH request.
	 *
	 * @param request - the http request
	 */
	void handleConnect(http::http_request request);

protected:

	void handleNotSupportedRequest(http::http_request request);
};

#endif /* SERVER_SRC_REQUEST_DEFAULTHTTPREQUESTHANDLER_H_ */
