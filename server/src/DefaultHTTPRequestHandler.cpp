/*
 * DefaultHTTPRequestHandler.cpp
 *
 *  Created on: Aug 1, 2016
 *      Author: ruelsison
 */

#include "DefaultHTTPRequestHandler.h"

#include <cpprest/json.h>
#include <cpprest/containerstream.h>
#include <pplx/pplx.h>

using namespace pplx;
using namespace concurrency::streams;

//#include <iostream>
//#include <sstream>

using namespace std;
using namespace utility;


DefaultHTTPRequestHandler::DefaultHTTPRequestHandler()
	: HTTPRequestHandler() {

}

void DefaultHTTPRequestHandler::handleHead(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleGet(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handlePut(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handlePost(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleDelete(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleOptions(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handlePatch(http::http_request request) {
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleMerge(http::http_request request)
{
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleTrace(http::http_request request)
{
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleConnect(http::http_request request)
{
	handleNotSupportedRequest(request);
}

void DefaultHTTPRequestHandler::handleNotSupportedRequest(http::http_request request) {
	std::ostringstream ss;
	ss << "Server resource " << request.relative_uri().to_string() << " request is not supported." << std::endl;
	std::cout << ss.str();

	request.content_ready().then([request](http_request request)
	{
		std::cout << "====== START RAW REQUEST ======" << std::endl;
		std::cout << request.to_string() << std::endl;
		std::cout << "====== END RAW REQUEST ======" << std::endl;
	});

//	std::cout << "=== headers ===" << std::endl;
//	http_headers headers = request.headers();
//	std::for_each(std::begin(headers), std::end(headers),
//	[=](http_headers::const_reference kv)
//	{
//		std::cout << "header " << kv.first << ": " << kv.second << std::endl;
//	});
//	std::cout << "=== headers ===" << std::endl;
//
//	concurrency::streams bodyStream = request.body();
//
//	container_buffer<std::string> inStringBuffer;
//	bodyStream.read_to_end(inStringBuffer).then([inStringBuffer](size_t bytesRead)
//	{
//		const std::string &text = inStringBuffer.collection();
//		std::cout << text << std::endl;
//	});

	utility::string_t message = "Resource ";
	message.append(request.relative_uri().to_string());
	message.append(" request is not supported.");



	json::value val;
	val["message"] = json::value::string(message);

	request.reply(status_codes::BadRequest, val);
}

