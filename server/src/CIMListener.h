/*
 * CIMListener.h
 *
 *  Created on: Jul 29, 2016
 *      Author: ruelsison
 */

#ifndef SERVER_CIMLISTENER_H_
#define SERVER_CIMLISTENER_H_

#include <cpprest/http_listener.h>
#include <cpprest/json.h>
#include <pplx/pplx.h>
//#pragma comment(lib, "cpprest110_1_1")

using namespace web;
using namespace web::http;
using namespace web::http::experimental::listener;

#include <iostream>
#include <map>
#include <set>
#include <string>
using namespace std;

#include "CIMRequestHandlerFactory.h"

// <summary>
/// TODO
/// </summary>
class CIMListener {
public:

	CIMListener(utility::string_t url);
	virtual ~CIMListener();

	pplx::task<void> open();
	pplx::task<void> close();

private:
	CIMListener();
	void handleRequest(http::http_request request);

	http_listener m_listener;
	CIMRequestHandlerFactory m_requestHandlerFactory;
};

#endif /* SERVER_CIMLISTENER_H_ */
