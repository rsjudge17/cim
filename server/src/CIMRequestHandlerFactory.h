/*
 * CIMRequestHandlerFactory.h
 *
 *  Created on: Jul 29, 2016
 *      Author: ruelsison
 */

#ifndef SERVER_CIMREQUESTHANDLERFACTORY_H_
#define SERVER_CIMREQUESTHANDLERFACTORY_H_

#include <cpprest/http_listener.h>

using namespace web;
using namespace web::http;

#include <string>
using namespace std;

class HTTPRequestHandler;

class CIMRequestHandlerFactory
{
public:
	CIMRequestHandlerFactory(utility::string_t format):
        _format(format)
    {
    }

    virtual ~CIMRequestHandlerFactory() {}

    HTTPRequestHandler* createRequestHandler(
        http::http_request request);

private:
    utility::string_t _format;
};

#endif /* SERVER_CIMREQUESTHANDLERFACTORY_H_ */
