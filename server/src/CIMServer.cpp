#include "CIMServer.h"

#include "Poco/Util/OptionCallback.h"
#include "Poco/DateTimeFormat.h"
#include "Poco/Exception.h"
#include "Poco/Util/HelpFormatter.h"

#include "CIMListener.h"

using Poco::Util::OptionCallback;
using Poco::DateTimeFormat;
using Poco::Util::HelpFormatter;

void CIMServer::initialize(Application& self) {
	loadConfiguration();
	ServerApplication::initialize(self);
}

void CIMServer::uninitialize() {
	ServerApplication::uninitialize();
}

void CIMServer::defineOptions(OptionSet& options) {
	ServerApplication::defineOptions(options);

	options.addOption(
			Option("help", "h", "display argument help information").required(
					false).repeatable(false).callback(
					OptionCallback<CIMServer>(this, &CIMServer::handleHelp)));
}

void CIMServer::handleHelp(const std::string& name, const std::string& value) {
	HelpFormatter helpFormatter(options());
	helpFormatter.setCommand(commandName());
	helpFormatter.setUsage("OPTIONS");
	helpFormatter.setHeader(
			"A web server that serves the current date and time.");
	helpFormatter.format(std::cout);
	stopOptionsProcessing();
	_helpRequested = true;
}

int CIMServer::main(const std::vector<std::string>& args) {
	if (!_helpRequested) {
		utility::string_t port = U("8068");
		utility::string_t endpoint = U("http://localhost:");
		endpoint.append(port);

		uri_builder uri(endpoint);
		uri.append_path(U("cimserver"));

		auto address = uri.to_uri().to_string();
		CIMListener listener(address);

		try {
			listener.open();
			cout << "CIMServer service address: " << address << endl;
			waitForTerminationRequest();
		} catch (const http_exception& ex) {
			Application& app = Application::instance();
			app.logger().error(ex.what());

			return Application::EXIT_SOFTWARE;
		}
		listener.close();
	}
	return Application::EXIT_OK;
}
