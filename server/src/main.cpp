/*
 * main.cpp
 *
 *  Created on: Jul 30, 2016
 *      Author: ruelsison
 */
#include "CIMServer.h"

#ifdef _WIN32
int wmain(int argc, wchar_t *argv[])
#else
int main(int argc, char *argv[])
#endif
{
	CIMServer server;
	return server.run(argc, argv);
}


