/*
 * CIMServer.h
 *
 *  Created on: Jul 30, 2016
 *      Author: ruelsison
 */

#ifndef SERVER_CIMSERVER_H_
#define SERVER_CIMSERVER_H_

#include "Poco/Util/ServerApplication.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"

using Poco::Util::ServerApplication;
using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;

// <summary>
/// TODO
/// </summary>
class CIMServer: public Poco::Util::ServerApplication {
public:
	CIMServer() :
			_helpRequested(false) {
	}
	~CIMServer() {
	}

protected:
	void initialize(Application& self);

	void uninitialize();

	void defineOptions(OptionSet& options);

	void handleHelp(const std::string& name, const std::string& value);

	int main(const std::vector<std::string>& args);

private:
	bool _helpRequested;
};

#endif /* SERVER_CIMSERVER_H_ */
