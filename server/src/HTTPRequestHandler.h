/*
 * HTTPRequestHandler.h
 *
 *  Created on: Aug 1, 2016
 *      Author: ruelsison
 */

#ifndef SERVER_SRC_REQUEST_HTTPREQUESTHANDLER_H_
#define SERVER_SRC_REQUEST_HTTPREQUESTHANDLER_H_

#include <cpprest/http_listener.h>

using namespace web;
using namespace web::http;

// <summary>
/// Basic interface for a URI request handler.  Handlers implementations
/// must be reentrant.
/// </summary>
class HTTPRequestHandler {
public:
	HTTPRequestHandler() {}
	virtual ~HTTPRequestHandler() {}

	/**
	 * Callback method for HEAD request.
	 *
	 * @param request - the http request
	 */
	virtual void handleHead(http::http_request request) = 0;

	/**
	 * Callback method for GET request.
	 *
	 * @param request - the http request
	 */
	virtual void handleGet(http::http_request request) = 0;

	/**
	 * Callback method for PUT request.
	 *
	 * @param request - the http request
	 */
	virtual void handlePut(http::http_request request) = 0;

	/**
	 * Callback method for POST request.
	 *
	 * @param request - the http request
	 */
	virtual void handlePost(http::http_request request) = 0;

	/**
	 * Callback method for DELETE request.
	 *
	 * @param request - the http request
	 */
	virtual void handleDelete(http::http_request request) = 0;

	/**
	 * Callback method for OPTIONS request.
	 *
	 * @param request - the http request
	 */
	virtual void handleOptions(http::http_request request) = 0;

	/**
	 * Callback method for PATCH request.
	 *
	 * @param request - the http request
	 */
	virtual void handlePatch(http::http_request request) = 0;

	/**
	 * Callback method for MERGE request.
	 *
	 * @param request - the http request
	 */
	virtual void handleMerge(http::http_request request) = 0;

	/**
	 * Callback method for TRACE request.
	 *
	 * @param request - the http request
	 */
	virtual void handleTrace(http::http_request request) = 0;

	/**
	 * Callback method for CONNECT request.
	 *
	 * @param request - the http request
	 */
	virtual void handleConnect(http::http_request request) = 0;
};

#endif /* SERVER_SRC_REQUEST_HTTPREQUESTHANDLER_H_ */
