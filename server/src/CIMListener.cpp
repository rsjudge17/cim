/*
 * CIMListener.cpp
 *
 *  Created on: Jul 29, 2016
 *      Author: ruelsison
 */

#include "CIMListener.h"
#include "HTTPRequestHandler.h"

#include <cpprest/http_headers.h>

using namespace web;
using namespace web::http;

CIMListener::CIMListener()
	: m_listener("")
	, m_requestHandlerFactory("")
{

}

CIMListener::CIMListener(utility::string_t url)
	: m_listener(url)
	, m_requestHandlerFactory(url)
{
	m_listener.support(methods::GET,
			std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::PUT,
			std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::POST,
			std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::DEL,
			std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::HEAD,
				std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::OPTIONS,
				std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::PATCH,
				std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::MERGE,
					std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::TRCE,
					std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
	m_listener.support(methods::CONNECT,
					std::bind(&CIMListener::handleRequest, this, std::placeholders::_1));
}

pplx::task<void> CIMListener::open() {
	return m_listener.open();
}

pplx::task<void> CIMListener::close() {
	return m_listener.close();
}

CIMListener::~CIMListener() {
	// TODO Auto-generated destructor stub
}

void CIMListener::handleRequest(http::http_request request)
{
//	std::ostringstream ss;
//	ss << "CIMListener::handleRequest" << std::endl;
//	ss << "request.relative_uri " << request.relative_uri().to_string() << std::endl;
//	ss << "request.method " << request.method() << std::endl;
//	std::cout << ss.str();
//
//	std::cout << "=== headers ===" << std::endl;
//	http_headers headers = request.headers();
//	std::for_each(std::begin(headers), std::end(headers),
//	[=](http_headers::const_reference kv)
//	{
//		std::cout << kv.first << ": " << kv.second << std::endl;
//	});
//	std::cout << "=== headers ===" << std::endl;

	HTTPRequestHandler* requestHandler = m_requestHandlerFactory.createRequestHandler(request);
	if (request.method() == methods::GET) {
		requestHandler->handleGet(request);
	} else if (request.method() == methods::PUT) {
		requestHandler->handlePut(request);
	} else if (request.method() == methods::POST) {
		requestHandler->handlePost(request);
	} else if (request.method() == methods::DEL) {
		requestHandler->handleGet(request);
	} else if (request.method() == methods::HEAD) {
		requestHandler->handleHead(request);
	} else if (request.method() == methods::OPTIONS) {
		requestHandler->handleOptions(request);
	} else if (request.method() == methods::MERGE) {
		requestHandler->handleMerge(request);
	} else if (request.method() == methods::TRCE) {
		requestHandler->handleTrace(request);
	} else if (request.method() == methods::PATCH) {
		requestHandler->handlePatch(request);
	} else if (request.method() == methods::CONNECT) {
		requestHandler->handleConnect(request);
	} else {
		std::ostringstream ss;
		ss << "Server method " << request.method() << " request is not supported." << std::endl;
		std::cout << ss.str();
	}

	if (requestHandler) {
		delete requestHandler;
	}
}

