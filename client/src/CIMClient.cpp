/*
 * CIMClient.cpp
 *
 *  Created on: Jul 31, 2016
 *      Author: ruelsison
 */

#include "CIMClient.h"

#include <pplx/pplx.h>
#include "Poco/Util/HelpFormatter.h"
#include "Poco/Util/AbstractConfiguration.h"
#include "Poco/AutoPtr.h"

using Poco::Util::HelpFormatter;
using Poco::Util::AbstractConfiguration;
using Poco::Util::OptionCallback;
using Poco::AutoPtr;

#include <pplx/pplx.h>
#include "cpprest/containerstream.h"
#if !defined(__GLIBCXX__)
#include <codecvt>
#endif

using namespace pplx;
using namespace concurrency::streams;
using namespace utility;

//#include <iostream>
//#include <sstream>



void CIMClient::initialize(Application& self) {
	loadConfiguration(); // load default configuration files, if present
	Application::initialize(self);
	// add your own initialization code here
}

void CIMClient::uninitialize() {
	// add your own uninitialization code here
	Application::uninitialize();
}

void CIMClient::reinitialize(Application& self) {
	Application::reinitialize(self);
	// add your own reinitialization code here
}

void CIMClient::defineOptions(OptionSet& options) {
	Application::defineOptions(options);

	options.addOption(
			Option("help", "h",
					"display help information on command line arguments").required(
					false).repeatable(false).callback(
					OptionCallback<CIMClient>(this, &CIMClient::handleHelp)));

	options.addOption(
			Option("define", "D", "define a configuration property").required(
					false).repeatable(true).argument("name=value").callback(
					OptionCallback<CIMClient>(this, &CIMClient::handleDefine)));

	options.addOption(
			Option("config-file", "f", "load configuration data from a file").required(
					false).repeatable(true).argument("file").callback(
					OptionCallback<CIMClient>(this, &CIMClient::handleConfig)));

	options.addOption(
			Option("bind", "b", "bind option value to test.property").required(
					false).repeatable(false).argument("value").binding(
					"test.property"));
}

void CIMClient::handleHelp(const std::string& name, const std::string& value) {
	_helpRequested = true;
	displayHelp();
	stopOptionsProcessing();
}

void CIMClient::handleDefine(const std::string& name,
		const std::string& value) {
	defineProperty(value);
}

void CIMClient::handleConfig(const std::string& name,
		const std::string& value) {
	loadConfiguration(value);
}

void CIMClient::displayHelp() {
	HelpFormatter helpFormatter(options());
	helpFormatter.setCommand(commandName());
	helpFormatter.setUsage("OPTIONS");
	helpFormatter.setHeader(
			"A sample application that demonstrates some of the features of the Poco::Util::Application class.");
	helpFormatter.format(std::cout);
}

void CIMClient::defineProperty(const std::string& def) {
	std::string name;
	std::string value;
	std::string::size_type pos = def.find('=');
	if (pos != std::string::npos) {
		name.assign(def, 0, pos);
		value.assign(def, pos + 1, def.length() - pos);
	} else
		name = def;
	config().setString(name, value);
}

pplx::task<void> CIMClient::HTTPStreamingAsync()
{
	utility::string_t address("http://localhost:8068/cimserver");
    http_client client(address);

    // Make the request and asynchronously process the response.
    return client.request(methods::GET).then([](http_response response)
    {
        // Print the status code.
        std::wostringstream ss;
        ss << L"Server returned returned status code " << response.status_code() << L'.' << std::endl;
        ss << L"Content length is " << response.headers().content_length() << L" bytes." << std::endl;
        std::wcout << ss.str();

        response.content_ready().then([response](http_response response)
		{
			std::cout << "====== START RAW RESPONSE ======" << std::endl;
			std::cout << response.to_string() << std::endl;
			std::cout << "====== END RAW RESPONSE ======" << std::endl;
		});

//        std::cout << "=== headers ===" << std::endl;
//		http_headers headers = response.headers();
//		std::for_each(std::begin(headers), std::end(headers),
//		[=](http_headers::const_reference kv)
//		{
//			std::cout << "header " << kv.first << ": " << kv.second << std::endl;
//		});
//		std::cout << "=== headers ===" << std::endl;
//
//        // TODO: Perform actions here reading from the response stream.
//        auto bodyStream = response.body();
//
//        container_buffer<std::string> inStringBuffer;
//		return bodyStream.read_to_end(inStringBuffer).then([inStringBuffer](size_t bytesRead)
//		{
//			const std::string &text = inStringBuffer.collection();
//			std::cout << text << std::endl;
//
//			// For demonstration, convert the response text to a wide-character string.
//			//std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> utf16conv;
//			//std::wostringstream ss;
//			//ss << utf16conv.from_bytes(text.c_str()) << std::endl;
//			//std::wcout << ss.str();
//		});
    });
}

int CIMClient::main(const ArgVec& args) {
	if (!_helpRequested) {
		logger().information("Command line:");
		std::ostringstream ostr;
		for (ArgVec::const_iterator it = argv().begin(); it != argv().end();
				++it) {
			ostr << *it << ' ';
		}
		logger().information(ostr.str());
		logger().information("Arguments to main():");
		for (ArgVec::const_iterator it = args.begin(); it != args.end(); ++it) {
			logger().information(*it);
		}
		logger().information("Application properties:");
		printProperties("");

		HTTPStreamingAsync().wait();

	}
	return Application::EXIT_OK;
}

void CIMClient::printProperties(const std::string& base) {
	AbstractConfiguration::Keys keys;
	config().keys(base, keys);
	if (keys.empty()) {
		if (config().hasProperty(base)) {
			std::string msg;
			msg.append(base);
			msg.append(" = ");
			msg.append(config().getString(base));
			logger().information(msg);
		}
	} else {
		for (AbstractConfiguration::Keys::const_iterator it = keys.begin();
				it != keys.end(); ++it) {
			std::string fullKey = base;
			if (!fullKey.empty())
				fullKey += '.';
			fullKey.append(*it);
			printProperties(fullKey);
		}
	}
}
