/*
 * CIMClient.h
 *
 *  Created on: Jul 31, 2016
 *      Author: ruelsison
 */

#ifndef CLIENT_CIMCLIENT_H_
#define CLIENT_CIMCLIENT_H_

#include <cpprest/http_client.h>

using namespace web;
using namespace web::http;
using namespace web::http::client;

#include "Poco/Util/Application.h"
#include "Poco/Util/Option.h"
#include "Poco/Util/OptionSet.h"


using Poco::Util::Application;
using Poco::Util::Option;
using Poco::Util::OptionSet;

// <summary>
/// TODO
/// </summary>
class CIMClient: public Application {
public:
	CIMClient(): _helpRequested(false)
	{
	}

protected:
	void initialize(Application& self);

	void uninitialize();

	void reinitialize(Application& self);

	void defineOptions(OptionSet& options);

	void handleHelp(const std::string& name, const std::string& value);

	void handleDefine(const std::string& name, const std::string& value);

	void handleConfig(const std::string& name, const std::string& value);

	void displayHelp();

	void defineProperty(const std::string& def);

	int main(const ArgVec& args);

	void printProperties(const std::string& base);

	pplx::task<void> HTTPStreamingAsync();

private:
	bool _helpRequested;
};

#endif /* CLIENT_CIMCLIENT_H_ */
