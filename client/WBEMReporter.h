/*
 * WBEMReporter.h
 *
 *  Created on: Jul 22, 2016
 *      Author: ruelsison
 */

#ifndef WBEMREPORTER_H_
#define WBEMREPORTER_H_

namespace wbem {

class WBEMReporter {
public:
	WBEMReporter();
	virtual ~WBEMReporter();
};

} /* namespace wbem */

#endif /* WBEMREPORTER_H_ */
