/*
 * WMIReader.h
 *
 *  Created on: Jul 22, 2016
 *      Author: ruelsison
 */

#ifndef WMIREADER_H_
#define WMIREADER_H_

namespace config {

class WMIReader {
public:
	WMIReader();
	virtual ~WMIReader();
};

} /* namespace config */

#endif /* WMIREADER_H_ */
