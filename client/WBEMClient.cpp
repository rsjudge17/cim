//============================================================================
// Name        : WBEMClient.cpp
// Author      : Ruel Sison
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
int wmain(int argc, wchar_t *argv[])
#else
int main(int argc, char *argv[])
#endif
{
	puts("Hello World!!!");
	return EXIT_SUCCESS;
}
